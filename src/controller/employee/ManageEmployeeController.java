/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.employee;

import controller.ManageController;
import controller.dao.DAODepartment;
import controller.dao.DAOEmployee;
import controller.dao.DAOSalaryGrade;
import controller.utils.ConnectionUtils;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import model.Department;
import model.Employee;
import model.SalaryGrade;
import view.ManageView;
import view.employee.AddEmployeeView;
import view.employee.ListEmployeeView;
import view.employee.ManageEmployeeView;
import view.employee.SearchEmployeeView;

/**
 *
 * @author Cuong Pham
 */
public class ManageEmployeeController {

    private DAOEmployee dAOEmployee;
    private DAODepartment dAODepartment;
    private DAOSalaryGrade dAOSalaryGrade;
    private ManageEmployeeView manageEmployeeView;
    private Connection conn;

    public ManageEmployeeController(ManageEmployeeView manageEmployeeView, Connection conn) {
        this.manageEmployeeView = manageEmployeeView;
        this.manageEmployeeView.addListener(new ManageEmployeeListener());

        this.conn = conn;
        dAOEmployee = new DAOEmployee(conn);
        dAODepartment = new DAODepartment(conn);
        dAOSalaryGrade = new DAOSalaryGrade(conn);
    }

    class ManageEmployeeListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == manageEmployeeView.getjButton4()) {
                manageEmployeeView.dispose();
                ListEmployeeView listEmployeeView = new ListEmployeeView();
                DefaultTableModel model = (DefaultTableModel) listEmployeeView.getjTable1().getModel();
                model.setRowCount(0);
                List<Employee> list = dAOEmployee.selectAll();
                for (Employee employee : list) {
                    model.addRow(employee.toObject());
                }
                ListEmployeeController listEmployeeController = new ListEmployeeController(listEmployeeView, conn);
                listEmployeeView.setVisible(true);
            }

            if (e.getSource() == manageEmployeeView.getjButton1()) {
                manageEmployeeView.dispose();
                List<Department> departments = dAODepartment.selectAll();
                List<SalaryGrade> salaryGrades = dAOSalaryGrade.selectAll();
                AddEmployeeView addEmployeeView = new AddEmployeeView();

                addEmployeeView.getjComboBox1().removeAllItems();
                addEmployeeView.getjComboBox2().removeAllItems();

                for (Department department : departments) {
                    addEmployeeView.getjComboBox1().addItem(department.getDeptId() + "");
                }
                for (SalaryGrade salaryGrade : salaryGrades) {
                    addEmployeeView.getjComboBox2().addItem(salaryGrade.getGrade() + "");
                }
                AddEmployeeController addEmployeeController = new AddEmployeeController(conn, addEmployeeView);
                addEmployeeView.setVisible(true);

            }

            if (e.getSource() == manageEmployeeView.getjButton3()) {
                manageEmployeeView.dispose();
                SearchEmployeeView searchEmployeeView = new SearchEmployeeView();
                SearchEmployeeController searchEmployeeController = new SearchEmployeeController(true, conn, searchEmployeeView);
                searchEmployeeView.setVisible(true);
            }

            if (e.getSource() == manageEmployeeView.getjButton2()) {
                manageEmployeeView.dispose();
                SearchEmployeeView searchEmployeeView = new SearchEmployeeView();
                SearchEmployeeController searchEmployeeController = new SearchEmployeeController(false, conn, searchEmployeeView);
                searchEmployeeView.setVisible(true);
            }

            if (e.getSource() == manageEmployeeView.getjButton5()) {
                manageEmployeeView.dispose();
                ManageView manageView = new ManageView();
                ManageController manageController = new ManageController(manageView, conn);
                manageView.setVisible(true);
            }
        }

    }

}
