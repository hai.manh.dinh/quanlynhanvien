/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.employee;

import controller.dao.DAOEmployee;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import model.Department;
import model.Employee;
import model.SalaryGrade;
import view.employee.AddEmployeeView;
import view.employee.ListEmployeeView;
import view.employee.ManageEmployeeView;

/**
 *
 * @author Cuong Pham
 */
public class AddEmployeeController {

    private DAOEmployee dAOEmployee;
    private Connection conn;
    private AddEmployeeView addEmployeeView;

    public AddEmployeeController(Connection conn, AddEmployeeView addEmployeeView) {
        this.conn = conn;
        this.dAOEmployee = new DAOEmployee(conn);
        this.addEmployeeView = addEmployeeView;
        this.addEmployeeView.addListener(new AddEmployeeListener());
    }

    class AddEmployeeListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == addEmployeeView.getjButton1()) {
                Employee employee = new Employee();
                employee.setEmpName(addEmployeeView.getjTextField1().getText());
                employee.setEmpNo(addEmployeeView.getjTextField2().getText());
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    employee.setHireDate(sdf.parse(addEmployeeView.getjTextField3().getText()));
                } catch (ParseException ex) {
                    Logger.getLogger(AddEmployeeController.class.getName()).log(Level.SEVERE, null, ex);
                }
                employee.setJob(addEmployeeView.getjTextField4().getText());
                employee.setSalary(Float.parseFloat(addEmployeeView.getjTextField5().getText()));
                employee.setMngId(Integer.parseInt(addEmployeeView.getjTextField6().getText()));
                employee.setDeptId(Integer.parseInt(addEmployeeView.getjComboBox1().getSelectedItem().toString()));
                employee.setGrdId(Integer.parseInt(addEmployeeView.getjComboBox2().getSelectedItem().toString()));
                employee.setImage(null);
                
                dAOEmployee.insert(employee);
                JOptionPane.showMessageDialog(addEmployeeView, "Bạn đã thêm nhân viên thành công");
                
                addEmployeeView.dispose();
                ManageEmployeeView manageEmployeeView = new ManageEmployeeView();
                ManageEmployeeController manageEmployeeController = new ManageEmployeeController(manageEmployeeView, conn);
                manageEmployeeView.setVisible(true);
                
            }
            
            if (e.getSource() == addEmployeeView.getjButton2()) {
                addEmployeeView.dispose();
                ManageEmployeeView manageEmployeeView = new ManageEmployeeView();
                ManageEmployeeController manageEmployeeController = new ManageEmployeeController(manageEmployeeView, conn);
                manageEmployeeView.setVisible(true);
            }

        }

    }

}
