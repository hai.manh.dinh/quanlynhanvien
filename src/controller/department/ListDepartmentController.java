/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.department;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import model.Department;
import view.department.ListDepartmentView;
import view.department.ManageDepartmentView;

/**
 *
 * @author Admin
 */
public class ListDepartmentController {
    private ListDepartmentView listDepartmentView;
    private Connection conn;

    public ListDepartmentController(ListDepartmentView listDepartmentView, Connection conn) {
        this.listDepartmentView = listDepartmentView;
        this.listDepartmentView.addListener(new ListDepartmentListener());
        this.conn = conn;
    }
    
    class ListDepartmentListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            listDepartmentView.dispose();
            ManageDepartmentView manageDepartmentView = new ManageDepartmentView();
            ManageDepartmentController manageDepartmentController = new ManageDepartmentController(manageDepartmentView, conn);
            manageDepartmentView.setVisible(true);
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
        
    }
}
