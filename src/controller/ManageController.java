/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import controller.department.ManageDepartmentController;
import controller.employee.ListEmployeeController;
import controller.employee.ManageEmployeeController;
import controller.timekeeper.ManageTimeKeeperController;
import controller.salary.ManagerSalaryController;
import controller.utils.ConnectionUtils;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import model.Employee;
import view.ManageView;
import view.department.ManageDepartmentView;
import view.salary.ManagerSalaryView;
import view.employee.ListEmployeeView;
import view.employee.ManageEmployeeView;
import view.timekeeper.ManageTimeKeeperView;

/**
 *
 * @author Cuong Pham
 */
public class ManageController {

    private ManageView manageView;
    private Connection conn;

    public ManageController(ManageView manageView, Connection conn) {
        this.manageView = manageView;
        this.manageView.addListener(new ManageListener());
        this.conn = conn;
    }

    class ManageListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == manageView.getjButton1()) {
                manageView.dispose();
                ManageEmployeeView manageEmployeeView = new ManageEmployeeView();
                ManageEmployeeController manageEmployeeController = new ManageEmployeeController(manageEmployeeView, conn);
                manageEmployeeView.setVisible(true);
            }

            if (e.getSource() == manageView.getjButton4()) {
                manageView.dispose();
                ManageTimeKeeperView manageTimeKeeperView = new ManageTimeKeeperView();
                ManageTimeKeeperController manageTimeKeeperController = new ManageTimeKeeperController(conn, manageTimeKeeperView);
                manageTimeKeeperView.setVisible(true);
            }

            if (e.getSource() == manageView.getjButton2()) {
                manageView.dispose();
                ManageDepartmentView manageDepartmentView = new ManageDepartmentView();
                ManageDepartmentController manageDepartmentController = new ManageDepartmentController(manageDepartmentView, conn);
                manageDepartmentView.setVisible(true);
            }

            if (e.getSource() == manageView.getjButton3()) {
                manageView.dispose();
                ManagerSalaryView managerSalaryView = new ManagerSalaryView();
                ManagerSalaryController managerSalaryController = new ManagerSalaryController(managerSalaryView,conn);               
                managerSalaryView.setVisible(true);

            }

        }

    }
}
