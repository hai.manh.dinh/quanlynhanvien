/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.salary;

import controller.ManageController;
import controller.dao.DAODepartment;
import controller.dao.DAOEmployee;
import controller.dao.DAOSalaryGrade;
import controller.utils.ConnectionUtils;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import model.Department;
import model.SalaryGrade;
import view.ManageView;
import view.salary.AddSalaryView;
import view.salary.ListSalaryView;
import view.salary.ManagerSalaryView;


/**
 *
 * @author Cuong Pham
 */
public class ManagerSalaryController {

    private DAOEmployee dAOEmployee;
    private DAODepartment dAODepartment;
    private DAOSalaryGrade dAOSalaryGrade;
    private ManagerSalaryView managerSalaryView;
    private Connection conn;

    public ManagerSalaryController(ManagerSalaryView managerSalaryView, Connection conn) {
        this.managerSalaryView = managerSalaryView;
        this.managerSalaryView.addListener(new ManagerSalaryListener());

        this.conn = conn;
        dAOEmployee = new DAOEmployee(conn);
        dAODepartment = new DAODepartment(conn);
        dAOSalaryGrade = new DAOSalaryGrade(conn);
    }

    class ManagerSalaryListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == managerSalaryView.getjButton4()) {
                managerSalaryView.dispose();
                ListSalaryView listSalaryView = new ListSalaryView();
                DefaultTableModel model = (DefaultTableModel) listSalaryView.getjTable1().getModel();
                model.setRowCount(0);
                List<SalaryGrade> list = dAOSalaryGrade.selectAll();
                System.out.println(list.size());
                for (SalaryGrade salary : list) {
                    model.addRow(salary.toObject());
                }
                ListSalaryController listSalaryController = new ListSalaryController(listSalaryView, conn);
                listSalaryView.setVisible(true);
            }
            if (e.getSource() == managerSalaryView.getjButton1()) {
                managerSalaryView.dispose();
                List<SalaryGrade> salaryGrades = dAOSalaryGrade.selectAll();
                AddSalaryView addSalaryView = new AddSalaryView();
                AddSalaryController addSalaryController = new AddSalaryController(conn, addSalaryView);
                addSalaryView.setVisible(true);

            }
            
            if (e.getSource() == managerSalaryView.getjButton5()) {
                managerSalaryView.dispose();
                ManageView manageView = new ManageView();
                ManageController manageController = new ManageController(manageView, conn);
                manageView.setVisible(true);
            }
        }

    }

}
