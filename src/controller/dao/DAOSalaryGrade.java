/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Department;
import model.Employee;
import model.SalaryGrade;

/**
 *
 * @author Cuong Pham
 */
public class DAOSalaryGrade extends IDAO<SalaryGrade> {

    public DAOSalaryGrade(Connection conn) {
        this.conn = conn;
        try {
            this.statement = this.conn.createStatement();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<SalaryGrade> selectAll() {
        List<SalaryGrade> result = new ArrayList<>();
        try {
            String sql = "SELECT * FROM SALARY_GRADE";

            rs = statement.executeQuery(sql);
            while (rs.next()) {
                SalaryGrade s = new SalaryGrade(rs.getInt(1), rs.getFloat(2), rs.getFloat(3));
                result.add(s);
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }

        return result;
    }

    @Override
    public List<SalaryGrade> selectByName(String name) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int insert(SalaryGrade salaryGrade) {
        String sql = "INSERT INTO SALARY_GRADE (HIGH_SALARY, LOW_SALARY) VALUES (?,?)";
        try {
            this.preStatement = this.conn.prepareStatement(sql);
            this.preStatement.setFloat(1, salaryGrade.getHighSalary());
            this.preStatement.setFloat(2, salaryGrade.getLowSalary());
            
            int rowCount = this.preStatement.executeUpdate();
            return rowCount;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return 0;
        }
    }

    @Override
    public int update(SalaryGrade object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void closeConnection() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    


}
